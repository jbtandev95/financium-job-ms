package com.jbtan.financium.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jbtan.financium.IntegrationTest;
import com.jbtan.financium.domain.MigrationTask;
import com.jbtan.financium.domain.enumeration.MigrationTaskStatus;
import com.jbtan.financium.domain.enumeration.Source;
import com.jbtan.financium.repository.MigrationTaskRepository;
import com.jbtan.financium.service.dto.MigrationTaskDTO;
import com.jbtan.financium.service.mapper.MigrationTaskMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MigrationTaskResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MigrationTaskResourceIT {

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    private static final Long DEFAULT_WALLET_ID = 1L;
    private static final Long UPDATED_WALLET_ID = 2L;

    private static final Source DEFAULT_SOURCE = Source.SPENDEE;
    private static final Source UPDATED_SOURCE = Source.SPENDEE;

    private static final MigrationTaskStatus DEFAULT_STATUS = MigrationTaskStatus.RUNNING;
    private static final MigrationTaskStatus UPDATED_STATUS = MigrationTaskStatus.COMPLETED;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/migration-tasks";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MigrationTaskRepository migrationTaskRepository;

    @Autowired
    private MigrationTaskMapper migrationTaskMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMigrationTaskMockMvc;

    private MigrationTask migrationTask;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MigrationTask createEntity(EntityManager em) {
        MigrationTask migrationTask = new MigrationTask()
            .userId(DEFAULT_USER_ID)
            .walletId(DEFAULT_WALLET_ID)
            .source(DEFAULT_SOURCE)
            .status(DEFAULT_STATUS)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY);
        return migrationTask;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MigrationTask createUpdatedEntity(EntityManager em) {
        MigrationTask migrationTask = new MigrationTask()
            .userId(UPDATED_USER_ID)
            .walletId(UPDATED_WALLET_ID)
            .source(UPDATED_SOURCE)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);
        return migrationTask;
    }

    @BeforeEach
    public void initTest() {
        migrationTask = createEntity(em);
    }

    @Test
    @Transactional
    void createMigrationTask() throws Exception {
        int databaseSizeBeforeCreate = migrationTaskRepository.findAll().size();
        // Create the MigrationTask
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);
        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isCreated());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeCreate + 1);
        MigrationTask testMigrationTask = migrationTaskList.get(migrationTaskList.size() - 1);
        assertThat(testMigrationTask.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testMigrationTask.getWalletId()).isEqualTo(DEFAULT_WALLET_ID);
        assertThat(testMigrationTask.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testMigrationTask.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testMigrationTask.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testMigrationTask.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMigrationTask.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testMigrationTask.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void createMigrationTaskWithExistingId() throws Exception {
        // Create the MigrationTask with an existing ID
        migrationTask.setId(1L);
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        int databaseSizeBeforeCreate = migrationTaskRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = migrationTaskRepository.findAll().size();
        // set the field null
        migrationTask.setUserId(null);

        // Create the MigrationTask, which fails.
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkWalletIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = migrationTaskRepository.findAll().size();
        // set the field null
        migrationTask.setWalletId(null);

        // Create the MigrationTask, which fails.
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSourceIsRequired() throws Exception {
        int databaseSizeBeforeTest = migrationTaskRepository.findAll().size();
        // set the field null
        migrationTask.setSource(null);

        // Create the MigrationTask, which fails.
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = migrationTaskRepository.findAll().size();
        // set the field null
        migrationTask.setStatus(null);

        // Create the MigrationTask, which fails.
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = migrationTaskRepository.findAll().size();
        // set the field null
        migrationTask.setCreatedDate(null);

        // Create the MigrationTask, which fails.
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = migrationTaskRepository.findAll().size();
        // set the field null
        migrationTask.setCreatedBy(null);

        // Create the MigrationTask, which fails.
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLastModifiedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = migrationTaskRepository.findAll().size();
        // set the field null
        migrationTask.setLastModifiedDate(null);

        // Create the MigrationTask, which fails.
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLastModifiedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = migrationTaskRepository.findAll().size();
        // set the field null
        migrationTask.setLastModifiedBy(null);

        // Create the MigrationTask, which fails.
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        restMigrationTaskMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMigrationTasks() throws Exception {
        // Initialize the database
        migrationTaskRepository.saveAndFlush(migrationTask);

        // Get all the migrationTaskList
        restMigrationTaskMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(migrationTask.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].walletId").value(hasItem(DEFAULT_WALLET_ID.intValue())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)));
    }

    @Test
    @Transactional
    void getMigrationTask() throws Exception {
        // Initialize the database
        migrationTaskRepository.saveAndFlush(migrationTask);

        // Get the migrationTask
        restMigrationTaskMockMvc
            .perform(get(ENTITY_API_URL_ID, migrationTask.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(migrationTask.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()))
            .andExpect(jsonPath("$.walletId").value(DEFAULT_WALLET_ID.intValue()))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY));
    }

    @Test
    @Transactional
    void getNonExistingMigrationTask() throws Exception {
        // Get the migrationTask
        restMigrationTaskMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingMigrationTask() throws Exception {
        // Initialize the database
        migrationTaskRepository.saveAndFlush(migrationTask);

        int databaseSizeBeforeUpdate = migrationTaskRepository.findAll().size();

        // Update the migrationTask
        MigrationTask updatedMigrationTask = migrationTaskRepository.findById(migrationTask.getId()).get();
        // Disconnect from session so that the updates on updatedMigrationTask are not directly saved in db
        em.detach(updatedMigrationTask);
        updatedMigrationTask
            .userId(UPDATED_USER_ID)
            .walletId(UPDATED_WALLET_ID)
            .source(UPDATED_SOURCE)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(updatedMigrationTask);

        restMigrationTaskMockMvc
            .perform(
                put(ENTITY_API_URL_ID, migrationTaskDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isOk());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeUpdate);
        MigrationTask testMigrationTask = migrationTaskList.get(migrationTaskList.size() - 1);
        assertThat(testMigrationTask.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testMigrationTask.getWalletId()).isEqualTo(UPDATED_WALLET_ID);
        assertThat(testMigrationTask.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testMigrationTask.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMigrationTask.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMigrationTask.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMigrationTask.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testMigrationTask.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void putNonExistingMigrationTask() throws Exception {
        int databaseSizeBeforeUpdate = migrationTaskRepository.findAll().size();
        migrationTask.setId(count.incrementAndGet());

        // Create the MigrationTask
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMigrationTaskMockMvc
            .perform(
                put(ENTITY_API_URL_ID, migrationTaskDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMigrationTask() throws Exception {
        int databaseSizeBeforeUpdate = migrationTaskRepository.findAll().size();
        migrationTask.setId(count.incrementAndGet());

        // Create the MigrationTask
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMigrationTaskMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMigrationTask() throws Exception {
        int databaseSizeBeforeUpdate = migrationTaskRepository.findAll().size();
        migrationTask.setId(count.incrementAndGet());

        // Create the MigrationTask
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMigrationTaskMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMigrationTaskWithPatch() throws Exception {
        // Initialize the database
        migrationTaskRepository.saveAndFlush(migrationTask);

        int databaseSizeBeforeUpdate = migrationTaskRepository.findAll().size();

        // Update the migrationTask using partial update
        MigrationTask partialUpdatedMigrationTask = new MigrationTask();
        partialUpdatedMigrationTask.setId(migrationTask.getId());

        partialUpdatedMigrationTask
            .walletId(UPDATED_WALLET_ID)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);

        restMigrationTaskMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMigrationTask.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMigrationTask))
            )
            .andExpect(status().isOk());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeUpdate);
        MigrationTask testMigrationTask = migrationTaskList.get(migrationTaskList.size() - 1);
        assertThat(testMigrationTask.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testMigrationTask.getWalletId()).isEqualTo(UPDATED_WALLET_ID);
        assertThat(testMigrationTask.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testMigrationTask.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMigrationTask.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMigrationTask.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMigrationTask.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testMigrationTask.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void fullUpdateMigrationTaskWithPatch() throws Exception {
        // Initialize the database
        migrationTaskRepository.saveAndFlush(migrationTask);

        int databaseSizeBeforeUpdate = migrationTaskRepository.findAll().size();

        // Update the migrationTask using partial update
        MigrationTask partialUpdatedMigrationTask = new MigrationTask();
        partialUpdatedMigrationTask.setId(migrationTask.getId());

        partialUpdatedMigrationTask
            .userId(UPDATED_USER_ID)
            .walletId(UPDATED_WALLET_ID)
            .source(UPDATED_SOURCE)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);

        restMigrationTaskMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMigrationTask.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMigrationTask))
            )
            .andExpect(status().isOk());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeUpdate);
        MigrationTask testMigrationTask = migrationTaskList.get(migrationTaskList.size() - 1);
        assertThat(testMigrationTask.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testMigrationTask.getWalletId()).isEqualTo(UPDATED_WALLET_ID);
        assertThat(testMigrationTask.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testMigrationTask.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMigrationTask.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMigrationTask.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMigrationTask.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testMigrationTask.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingMigrationTask() throws Exception {
        int databaseSizeBeforeUpdate = migrationTaskRepository.findAll().size();
        migrationTask.setId(count.incrementAndGet());

        // Create the MigrationTask
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMigrationTaskMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, migrationTaskDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMigrationTask() throws Exception {
        int databaseSizeBeforeUpdate = migrationTaskRepository.findAll().size();
        migrationTask.setId(count.incrementAndGet());

        // Create the MigrationTask
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMigrationTaskMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMigrationTask() throws Exception {
        int databaseSizeBeforeUpdate = migrationTaskRepository.findAll().size();
        migrationTask.setId(count.incrementAndGet());

        // Create the MigrationTask
        MigrationTaskDTO migrationTaskDTO = migrationTaskMapper.toDto(migrationTask);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMigrationTaskMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(migrationTaskDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MigrationTask in the database
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMigrationTask() throws Exception {
        // Initialize the database
        migrationTaskRepository.saveAndFlush(migrationTask);

        int databaseSizeBeforeDelete = migrationTaskRepository.findAll().size();

        // Delete the migrationTask
        restMigrationTaskMockMvc
            .perform(delete(ENTITY_API_URL_ID, migrationTask.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MigrationTask> migrationTaskList = migrationTaskRepository.findAll();
        assertThat(migrationTaskList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
