package com.jbtan.financium.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.jbtan.financium.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MigrationTaskTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MigrationTask.class);
        MigrationTask migrationTask1 = new MigrationTask();
        migrationTask1.setId(1L);
        MigrationTask migrationTask2 = new MigrationTask();
        migrationTask2.setId(migrationTask1.getId());
        assertThat(migrationTask1).isEqualTo(migrationTask2);
        migrationTask2.setId(2L);
        assertThat(migrationTask1).isNotEqualTo(migrationTask2);
        migrationTask1.setId(null);
        assertThat(migrationTask1).isNotEqualTo(migrationTask2);
    }
}
