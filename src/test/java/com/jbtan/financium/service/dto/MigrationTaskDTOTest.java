package com.jbtan.financium.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jbtan.financium.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MigrationTaskDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MigrationTaskDTO.class);
        MigrationTaskDTO migrationTaskDTO1 = new MigrationTaskDTO();
        migrationTaskDTO1.setId(1L);
        MigrationTaskDTO migrationTaskDTO2 = new MigrationTaskDTO();
        assertThat(migrationTaskDTO1).isNotEqualTo(migrationTaskDTO2);
        migrationTaskDTO2.setId(migrationTaskDTO1.getId());
        assertThat(migrationTaskDTO1).isEqualTo(migrationTaskDTO2);
        migrationTaskDTO2.setId(2L);
        assertThat(migrationTaskDTO1).isNotEqualTo(migrationTaskDTO2);
        migrationTaskDTO1.setId(null);
        assertThat(migrationTaskDTO1).isNotEqualTo(migrationTaskDTO2);
    }
}
