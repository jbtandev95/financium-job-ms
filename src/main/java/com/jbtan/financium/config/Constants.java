package com.jbtan.financium.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String SYSTEM = "system";

    private Constants() {}
}
