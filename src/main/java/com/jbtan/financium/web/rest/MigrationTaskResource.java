package com.jbtan.financium.web.rest;

import com.jbtan.financium.repository.MigrationTaskRepository;
import com.jbtan.financium.service.MigrationTaskService;
import com.jbtan.financium.service.dto.MigrationTaskDTO;
import com.jbtan.financium.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jbtan.financium.domain.MigrationTask}.
 */
@RestController
@RequestMapping("/api")
public class MigrationTaskResource {

    private final Logger log = LoggerFactory.getLogger(MigrationTaskResource.class);

    private static final String ENTITY_NAME = "jobMigrationTask";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MigrationTaskService migrationTaskService;

    private final MigrationTaskRepository migrationTaskRepository;

    public MigrationTaskResource(MigrationTaskService migrationTaskService, MigrationTaskRepository migrationTaskRepository) {
        this.migrationTaskService = migrationTaskService;
        this.migrationTaskRepository = migrationTaskRepository;
    }

    /**
     * {@code POST  /migration-tasks} : Create a new migrationTask.
     *
     * @param migrationTaskDTO the migrationTaskDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new migrationTaskDTO, or with status {@code 400 (Bad Request)} if the migrationTask has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/migration-tasks")
    public ResponseEntity<MigrationTaskDTO> createMigrationTask(@Valid @RequestBody MigrationTaskDTO migrationTaskDTO)
        throws URISyntaxException {
        log.debug("REST request to save MigrationTask : {}", migrationTaskDTO);
        if (migrationTaskDTO.getId() != null) {
            throw new BadRequestAlertException("A new migrationTask cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MigrationTaskDTO result = migrationTaskService.save(migrationTaskDTO);
        return ResponseEntity
            .created(new URI("/api/migration-tasks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /migration-tasks/:id} : Updates an existing migrationTask.
     *
     * @param id the id of the migrationTaskDTO to save.
     * @param migrationTaskDTO the migrationTaskDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated migrationTaskDTO,
     * or with status {@code 400 (Bad Request)} if the migrationTaskDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the migrationTaskDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/migration-tasks/{id}")
    public ResponseEntity<MigrationTaskDTO> updateMigrationTask(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MigrationTaskDTO migrationTaskDTO
    ) throws URISyntaxException {
        log.debug("REST request to update MigrationTask : {}, {}", id, migrationTaskDTO);
        if (migrationTaskDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, migrationTaskDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!migrationTaskRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MigrationTaskDTO result = migrationTaskService.update(migrationTaskDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, migrationTaskDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /migration-tasks/:id} : Partial updates given fields of an existing migrationTask, field will ignore if it is null
     *
     * @param id the id of the migrationTaskDTO to save.
     * @param migrationTaskDTO the migrationTaskDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated migrationTaskDTO,
     * or with status {@code 400 (Bad Request)} if the migrationTaskDTO is not valid,
     * or with status {@code 404 (Not Found)} if the migrationTaskDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the migrationTaskDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/migration-tasks/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MigrationTaskDTO> partialUpdateMigrationTask(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MigrationTaskDTO migrationTaskDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update MigrationTask partially : {}, {}", id, migrationTaskDTO);
        if (migrationTaskDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, migrationTaskDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!migrationTaskRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MigrationTaskDTO> result = migrationTaskService.partialUpdate(migrationTaskDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, migrationTaskDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /migration-tasks} : get all the migrationTasks.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of migrationTasks in body.
     */
    @GetMapping("/migration-tasks")
    public List<MigrationTaskDTO> getAllMigrationTasks() {
        log.debug("REST request to get all MigrationTasks");
        return migrationTaskService.findAll();
    }

    /**
     * {@code GET  /migration-tasks/:id} : get the "id" migrationTask.
     *
     * @param id the id of the migrationTaskDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the migrationTaskDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/migration-tasks/{id}")
    public ResponseEntity<MigrationTaskDTO> getMigrationTask(@PathVariable Long id) {
        log.debug("REST request to get MigrationTask : {}", id);
        Optional<MigrationTaskDTO> migrationTaskDTO = migrationTaskService.findOne(id);
        return ResponseUtil.wrapOrNotFound(migrationTaskDTO);
    }

    /**
     * {@code DELETE  /migration-tasks/:id} : delete the "id" migrationTask.
     *
     * @param id the id of the migrationTaskDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/migration-tasks/{id}")
    public ResponseEntity<Void> deleteMigrationTask(@PathVariable Long id) {
        log.debug("REST request to delete MigrationTask : {}", id);
        migrationTaskService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
