/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jbtan.financium.web.rest.vm;
