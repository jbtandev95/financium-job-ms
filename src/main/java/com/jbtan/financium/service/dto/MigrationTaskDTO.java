package com.jbtan.financium.service.dto;

import com.jbtan.financium.domain.enumeration.MigrationTaskStatus;
import com.jbtan.financium.domain.enumeration.Source;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jbtan.financium.domain.MigrationTask} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class MigrationTaskDTO implements Serializable {

    private Long id;

    @NotNull
    private Long userId;

    @NotNull
    private Long walletId;

    @NotNull
    private Source source;

    @NotNull
    private MigrationTaskStatus status;

    @NotNull
    private Instant createdDate;

    @NotNull
    private String createdBy;

    @NotNull
    private Instant lastModifiedDate;

    @NotNull
    private String lastModifiedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public MigrationTaskStatus getStatus() {
        return status;
    }

    public void setStatus(MigrationTaskStatus status) {
        this.status = status;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MigrationTaskDTO)) {
            return false;
        }

        MigrationTaskDTO migrationTaskDTO = (MigrationTaskDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, migrationTaskDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MigrationTaskDTO{" +
            "id=" + getId() +
            ", userId=" + getUserId() +
            ", walletId=" + getWalletId() +
            ", source='" + getSource() + "'" +
            ", status='" + getStatus() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            "}";
    }
}
