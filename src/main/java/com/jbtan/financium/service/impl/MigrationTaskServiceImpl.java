package com.jbtan.financium.service.impl;

import com.jbtan.financium.domain.MigrationTask;
import com.jbtan.financium.repository.MigrationTaskRepository;
import com.jbtan.financium.service.MigrationTaskService;
import com.jbtan.financium.service.dto.MigrationTaskDTO;
import com.jbtan.financium.service.mapper.MigrationTaskMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link MigrationTask}.
 */
@Service
@Transactional
public class MigrationTaskServiceImpl implements MigrationTaskService {

    private final Logger log = LoggerFactory.getLogger(MigrationTaskServiceImpl.class);

    private final MigrationTaskRepository migrationTaskRepository;

    private final MigrationTaskMapper migrationTaskMapper;

    public MigrationTaskServiceImpl(MigrationTaskRepository migrationTaskRepository, MigrationTaskMapper migrationTaskMapper) {
        this.migrationTaskRepository = migrationTaskRepository;
        this.migrationTaskMapper = migrationTaskMapper;
    }

    @Override
    public MigrationTaskDTO save(MigrationTaskDTO migrationTaskDTO) {
        log.debug("Request to save MigrationTask : {}", migrationTaskDTO);
        MigrationTask migrationTask = migrationTaskMapper.toEntity(migrationTaskDTO);
        migrationTask = migrationTaskRepository.save(migrationTask);
        return migrationTaskMapper.toDto(migrationTask);
    }

    @Override
    public MigrationTaskDTO update(MigrationTaskDTO migrationTaskDTO) {
        log.debug("Request to update MigrationTask : {}", migrationTaskDTO);
        MigrationTask migrationTask = migrationTaskMapper.toEntity(migrationTaskDTO);
        migrationTask = migrationTaskRepository.save(migrationTask);
        return migrationTaskMapper.toDto(migrationTask);
    }

    @Override
    public Optional<MigrationTaskDTO> partialUpdate(MigrationTaskDTO migrationTaskDTO) {
        log.debug("Request to partially update MigrationTask : {}", migrationTaskDTO);

        return migrationTaskRepository
            .findById(migrationTaskDTO.getId())
            .map(existingMigrationTask -> {
                migrationTaskMapper.partialUpdate(existingMigrationTask, migrationTaskDTO);

                return existingMigrationTask;
            })
            .map(migrationTaskRepository::save)
            .map(migrationTaskMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MigrationTaskDTO> findAll() {
        log.debug("Request to get all MigrationTasks");
        return migrationTaskRepository.findAll().stream().map(migrationTaskMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MigrationTaskDTO> findOne(Long id) {
        log.debug("Request to get MigrationTask : {}", id);
        return migrationTaskRepository.findById(id).map(migrationTaskMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete MigrationTask : {}", id);
        migrationTaskRepository.deleteById(id);
    }
}
