package com.jbtan.financium.service.mapper;

import com.jbtan.financium.domain.MigrationTask;
import com.jbtan.financium.service.dto.MigrationTaskDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link MigrationTask} and its DTO {@link MigrationTaskDTO}.
 */
@Mapper(componentModel = "spring")
public interface MigrationTaskMapper extends EntityMapper<MigrationTaskDTO, MigrationTask> {}
