package com.jbtan.financium.service;

import com.jbtan.financium.service.dto.MigrationTaskDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.jbtan.financium.domain.MigrationTask}.
 */
public interface MigrationTaskService {
    /**
     * Save a migrationTask.
     *
     * @param migrationTaskDTO the entity to save.
     * @return the persisted entity.
     */
    MigrationTaskDTO save(MigrationTaskDTO migrationTaskDTO);

    /**
     * Updates a migrationTask.
     *
     * @param migrationTaskDTO the entity to update.
     * @return the persisted entity.
     */
    MigrationTaskDTO update(MigrationTaskDTO migrationTaskDTO);

    /**
     * Partially updates a migrationTask.
     *
     * @param migrationTaskDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<MigrationTaskDTO> partialUpdate(MigrationTaskDTO migrationTaskDTO);

    /**
     * Get all the migrationTasks.
     *
     * @return the list of entities.
     */
    List<MigrationTaskDTO> findAll();

    /**
     * Get the "id" migrationTask.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MigrationTaskDTO> findOne(Long id);

    /**
     * Delete the "id" migrationTask.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
