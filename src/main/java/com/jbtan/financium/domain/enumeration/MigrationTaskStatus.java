package com.jbtan.financium.domain.enumeration;

/**
 * The MigrationTaskStatus enumeration.
 */
public enum MigrationTaskStatus {
    RUNNING,
    COMPLETED,
    PENDING,
    FAILED,
}
