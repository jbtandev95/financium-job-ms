package com.jbtan.financium.repository;

import com.jbtan.financium.domain.MigrationTask;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the MigrationTask entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MigrationTaskRepository extends JpaRepository<MigrationTask, Long> {}
